import * as React from 'react'
import { Sidebar, Menu, Icon, Segment } from 'semantic-ui-react'

export default function Header( props ) {
    const { children, activeItem } = props

    return (
        <>
            <Sidebar.Pushable as={Segment}>
                <Sidebar
                as={Menu}
                animation='push'
                icon='labeled'
                inverted
                vertical
                visible
                width='thin'
                >
                <Menu.Item as='a'>
                    <Icon name='home' />
                    Home
                </Menu.Item>
                <Menu.Item as='a'>
                    <Icon name='gamepad' />
                    Games
                </Menu.Item>
                <Menu.Item as='a'>
                    <Icon name='camera' />
                    Channels
                </Menu.Item>
                </Sidebar>

                <Sidebar.Pusher className="min-vh-100">
                    <div className="row mx-0 py-3 bg-primary">
                        <div className="col-md-2">
                            <img src="https://picsum.photos/150/50" />
                        </div>
                    </div>
                    { children}
                </Sidebar.Pusher>
            </Sidebar.Pushable>
        </>
    )
}