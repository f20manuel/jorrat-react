import * as React from 'react'
import { Button, IconButton } from '@material-ui/core'
import HighlightOffRoundedIcon from '@material-ui/icons/HighlightOffRounded';

export default function HeaderAD(props) {
    const {
        children, // string or node content Ex: <Tag>content</Tag>
        title, // string (optional)
        type, // string [info|success|error|warning|danger], Default: "undefined" (optional)
        hasButton = false, // Boolean, Default: false
        buttonText, // string (optional)
        buttonLink = '#', // url, Default: '#'
        visible = true // Booleanm, Default: true
    } = props

    const [displayBlock, setDisplayBlock] = React.useState(visible)

    return (
        <>
        {displayBlock ? (
            <div className={"row mx-0 justify-content-center bg-"+type}>
                <div className="col-lg-6 py-3 py-md-0">
                    <div className="d-flex justify-content-between align-items-center">
                        <div className={type === 'danger' ? 'text-white' : 'text-dark'}>
                            <strong>{title ? title : null}</strong>{' '}{children}{' '}
                            {hasButton && buttonText ? (
                            <Button
                                href={buttonLink || '#'}
                                className="ml-3"
                                variant="outlined"
                                color={type === 'danger' ? 'secondary' : 'primary'}
                                size="small">
                                {buttonText}
                            </Button>
                            ) : null}
                        </div>
                        <div className="float-right">
                            <IconButton className={type === 'danger' ? 'text-white' : 'text-dark'} onClick={() => {
                                setDisplayBlock(false)
                            }}>
                                <HighlightOffRoundedIcon />
                            </IconButton>
                        </div>
                    </div>
                </div>
            </div>
        ) : null}
        </>
    )
}