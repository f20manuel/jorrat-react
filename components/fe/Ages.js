import React from 'react'
import { Container, Button } from '@material-ui/core'
import { primaryColor } from '../../enviroment'

export default function Ages(props) {
    const { title, data } = props
    return (
        <Container>
            <div className="row mx-0 justify-content-center my-3">
                <div className="col-lg-6 text-center">
                    <h1 style={{color: primaryColor}} className="title text-center">{title}</h1>
                </div>
            </div>
            <div className="row mx-0 justify-content-center my-5">
                {data.map((age, index) => (
                    <div
                        style={
                            {
                                backgroundImage: age.backgroundImage?'url('+age.backgroundImage+')':"url('https://via.placeholder.com/300x150')",
                                backgroundPosition: 'center center',
                                backgroundRepeat: 'no-repeat',
                                backgroundSize: 'cover',
                            }
                        }
                        key={index}
                        className="col-lg-3 p-3 mx-2 mb-3 mb-md-0">
                        <div className="row mx-0">
                            <div className="col-lg-4 text-center">
                                <h1 style={{color: primaryColor}} className="title mb-0">{age.title}</h1>
                                <h2 style={{color: primaryColor}} className="subtitle mt-n3 mb-3">{age.type}</h2>
                            </div>
                            <div className="col-12 text-center text-md-left">
                                <Button color="secondary" href={age.buttonLink} variant="contained">{age.buttonText}</Button>
                            </div>
                        </div>
                    </div>
                ))}
            </div>
        </Container>
    )
}
