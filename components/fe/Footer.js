import React from "react";
import { Container } from "@material-ui/core";
import { Image, Button } from "semantic-ui-react";
import { public_path } from "../../enviroment";

export default function Footer(props) {
  const { address } = props;
  return (
    <div className="bg-footer py-5 mt-5">
      <Container maxWidth="md">
        <div className="row mx-0 justify-content-center">
          <div className="col-md-3 text-center text-md-left mb-3">
            <img src={"./assets/img/brand/logo.png"} className="mb-3" />
            <p className="text-white">
              {address[0].titulo}, {address[0].sumario}
              <br />
              {address[0].ciudad}
              <br />
              Teléfono: 0381 431-1324
            </p>
          </div>
          <div className="col-md-3 text-center text-md-left mb-3">
            <h3 className="title-footer">INSTITUCIÓN</h3>
            <ul className="nav flex-column">
              <li className="nav-item">
                <a className="nav-link text-white" href="#">
                  Quienes somos
                </a>
              </li>
              <li className="nav-item">
                <a className="nav-link text-white" href="#">
                  Servicios
                </a>
              </li>
              <li className="nav-item">
                <a className="nav-link text-white" href="#">
                  Términos y condiciones
                </a>
              </li>
              <li className="nav-item">
                <a className="nav-link text-white" href="#">
                  Políticas de Privacidad
                </a>
              </li>
              <li className="nav-item">
                <a className="nav-link text-white" href="#">
                  Deja tu CV
                </a>
              </li>
            </ul>
          </div>
          <div className="col-md-3 text-center text-md-left mb-3">
            <h3 className="title-footer">AYUDA</h3>
            <ul className="nav flex-column">
              <li className="nav-item">
                <a className="nav-link text-white" href="#">
                  Cómo Comprar?
                </a>
              </li>
              <li className="nav-item">
                <a className="nav-link text-white" href="#">
                  Cambio de Mercadería
                </a>
              </li>
              <li className="nav-item">
                <a className="nav-link text-white" href="#">
                  Envíos
                </a>
              </li>
              <li className="nav-item">
                <a className="nav-link text-white" href="#">
                  Retiro de Sucursal
                </a>
              </li>
              <li className="nav-item">
                <a className="nav-link text-white" href="#">
                  Medios de Pago
                </a>
              </li>
            </ul>
          </div>
          <div className="col-md-3 text-center text-md-left mb-3">
            <h3 className="title-footer">SEGUINOS</h3>
            <div className="d-flex d-none justify-content-center align-items-center">
              <Button icon="facebook" className="bg-facebook rounded-circle" />
              <Button
                icon="instagram"
                className="bg-instagram rounded-circle"
              />
            </div>
          </div>
        </div>
      </Container>
    </div>
  );
}
