import React from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

export default function Features(props) {
    const { data } = props
    return (
        <div className="row mx-0 justify-content-center align-items-center my-5">
            {data.map((feature, index) => (
                <div key={index} className="col-md-2 text-center">
                    {feature.icono?(
                        <FontAwesomeIcon icon={feature.icono} />
                    ):(
                        <img src="https://via.placeholder.com/100" width={100} className="rounded-circle" alt={feature.titulo}/>
                    )}
                    <h4 className="mb-0 feature-title">{feature.titulo}</h4>
                    <small className="feature-description">{feature.sumario}</small>
                </div>
            ))}
        </div>
    )
}
