import React, { useEffect, useState, useRef } from "react";
import {
  TextField,
  IconButton,
  Menu,
  MenuItem,
  Divider,
} from "@material-ui/core";
import Autocomplete from "@material-ui/lab/Autocomplete";
import {
  secondaryColor,
  tercerColor,
  public_path,
  primaryColor,
  api_files_path,
  api,
} from "../../enviroment";
// icons
import DirectionsBusRoundedIcon from "@material-ui/icons/DirectionsBusRounded";
import DirectionsBikeRoundedIcon from "@material-ui/icons/DirectionsBikeRounded";
import SportsSoccerRoundedIcon from "@material-ui/icons/SportsSoccerRounded";
import SearchIcon from "@material-ui/icons/Search";
import ShoppingBasketRoundedIcon from "@material-ui/icons/ShoppingBasketRounded";
import AccountCircleRoundedIcon from "@material-ui/icons/AccountCircleRounded";
import { Image, Input, Button, Dropdown } from "semantic-ui-react";
import Router from "next/router";
// import Axios from 'axios'

function Header({ logoURL, tags, search, sliders, getMenu }) {
  const [user, setUser] = useState("");

  const [dropdown1, setDropdown1] = useState(false);
  const [dropdown2, setDropdown2] = useState(false);
  const [dropdown3, setDropdown3] = useState(false);

  // Login menu
  const [loginMenu, setLoginMenu] = useState(false);
  const handleLoginMenu = (event) => {
    setLoginMenu(event.currentTarget);
  };
  const handleCloseLoginMenu = () => {
    setLoginMenu(null);
  };

  const handleSearchIcon = () => {
    document.querySelector(
      "button.MuiAutocomplete-popupIndicator"
    ).style.transform = "rotate(0deg)";
  };

  useEffect(() => {
    document.querySelectorAll("i.dropdown").forEach((i) => {
      i.style.display = "none";
    });
  }, []);

  const logout = () => {
    // Axios.post(public_path('logout'), {}).then(res => {|
    //     alert('Gracias por su visita')
    //     window.location.href = public_path()
    // }).catch(error => console.log(error.response.data))
  };

  return (
    <div style={{ backgroundColor: secondaryColor }} className="sticky-top">
      <div className="row mx-0 justify-content-center align-items-center py-3">
        <div className="col-lg-2 text-center text-lg-left mb-3 mb-lg-0">
          <a href="/">
            <img src={logoURL} alt="logo" />
          </a>
        </div>
        <div className="col-md-10">
          <div className="row mx-0 justify-content-center">
            <div className="d-none col-12 text-center d-lg-flex align-items-center">
              {getMenu.map((menu, index) => (
                <div
                  key={index}
                  className="mr-4"
                  style={{ color: primaryColor }}
                >
                  {menu.id === 1 ? <DirectionsBusRoundedIcon /> : null}
                  {menu.id === 2 ? <DirectionsBikeRoundedIcon /> : null}
                  {menu.id === 3 ? <SportsSoccerRoundedIcon /> : null}
                  <br />
                  <Dropdown text={menu.nombre.split(" ")[0]}>
                    <Dropdown.Menu>
                      {typeof menu.subrubros !== "undefined" &&
                      menu.subrubros.length > 0
                        ? menu.subrubros.map((subrubro, indexSubrubro) => (
                            <Dropdown.Item>
                              <Dropdown
                                key={indexSubrubro}
                                text={subrubro.nombre}
                              >
                                <Dropdown.Menu>
                                  {typeof subrubro.subsubrubros !==
                                    "undefined" &&
                                  subrubro.subsubrubros.length > 0
                                    ? subrubro.subsubrubros.map(
                                        (subsubrubro, indexSubsubrubro) => (
                                          <Dropdown.Item key={indexSubsubrubro}>
                                            {subsubrubro.nombre}
                                          </Dropdown.Item>
                                        )
                                      )
                                    : null}
                                </Dropdown.Menu>
                              </Dropdown>
                            </Dropdown.Item>
                          ))
                        : null}
                    </Dropdown.Menu>
                  </Dropdown>
                </div>
              ))}
              <Autocomplete
                size="small"
                id="tags-outlined"
                fullWidth
                className="mr-5"
                loadingText="Cargando..."
                noOptionsText="No se encontraron resultados"
                limitTags={5}
                popupIcon={<SearchIcon />}
                options={search}
                getOptionLabel={(option) =>
                  option["id"] + " - " + option["titulo"]
                }
                // defaultValue={[products[0]]}
                // filterSelectedOptions
                clearOnEscape
                renderInput={(params) => (
                  <TextField
                    {...params}
                    fullWidth
                    style={{ backgroundColor: "white" }}
                    variant="outlined"
                    label="Buscar producto"
                    onFocus={() => handleSearchIcon()}
                    onBlur={() => handleSearchIcon()}
                    onClick={() => handleSearchIcon()}
                    placeholder="Buscar en Jorrat"
                  />
                )}
                onInputChange={(event, value) => (Router.push(`/producto/${value.split(' ')[0]}`))}
                renderOption={(option) => (
                  <div className="d-flex justify-content-between">
                    <Image
                      src={api_files_path + option["fotos"][0].imagen_file}
                      wrapped
                      style={{ width: 100 }}
                      className="mr-3"
                    />
                    <div>
                      <h3 className="my-0">{option["titulo"]}</h3>
                      <small style={{ color: tercerColor }}>
                        {option["categoria"].rubro.rubro},{" "}
                        {option["categoria"].subrubro.subrubro},{" "}
                        {option["categoria"].subsubrubro.subsubrubro}
                      </small>
                      {option["precios"].precio_lista !== "0" ? (
                        <div className="d-flex align-items-end">
                          <h3 className="mb-0 mr-2 text-gray">
                            <s>{option["precios"].precio_lista}</s>
                          </h3>
                          <h2 className="mt-0">
                            {option["precios"].precio_venta}
                          </h2>
                        </div>
                      ) : (
                        <div className="d-flex">
                          <h2 className="mt-0">
                            {option["precios"].precio_venta}
                          </h2>
                        </div>
                      )}
                    </div>
                  </div>
                )}
              />
              <div className="d-flex">
                <IconButton
                  className="mr-3 text-dark"
                  onClick={handleLoginMenu}
                >
                  <AccountCircleRoundedIcon />
                </IconButton>
                {user ? (
                  <Menu
                    anchorEl={loginMenu}
                    keepMounted
                    open={Boolean(loginMenu)}
                    onClose={handleCloseLoginMenu}
                  >
                    <MenuItem
                      onClick={() =>
                        (window.location.href = public_path("home"))
                      }
                    >
                      Cuenta
                    </MenuItem>
                    <Divider />
                    <MenuItem onClick={() => logout()}>Salir</MenuItem>
                  </Menu>
                ) : (
                  <Menu
                    anchorEl={loginMenu}
                    keepMounted
                    open={Boolean(loginMenu)}
                    onClose={handleCloseLoginMenu}
                  >
                    <MenuItem
                      onClick={() =>
                        (window.location.href = public_path("login"))
                      }
                    >
                      Acceder
                    </MenuItem>
                    <MenuItem
                      onClick={() =>
                        (window.location.href = public_path("register"))
                      }
                    >
                      Registrarse
                    </MenuItem>
                  </Menu>
                )}
                <IconButton className="text-dark">
                  <ShoppingBasketRoundedIcon />
                </IconButton>
              </div>
            </div>
            <div className="col-12 d-flex mt-3">
              {tags.map((tag, index) => (
                <a
                  href={tag.url}
                  key={index}
                  className={
                    "badge badge-" +
                    tag.type +
                    " text-decoration-none mr-2 text-white"
                  }
                >
                  {tag.title}
                </a>
              ))}
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Header;
