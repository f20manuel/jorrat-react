import React from 'react'
import { PropTypes } from 'prop-types'
import { sliderPrincipalBanner, api_files_path, secondaryColor } from '../../enviroment'
import Slider from 'react-slick'
import { Paper } from '@material-ui/core'
import { Button } from 'semantic-ui-react'
import { Router } from 'next/router'

function Banner({ sliders }) {
    return (
        <Slider {...sliderPrincipalBanner}>
            {sliders.map((slider, index) => (
                <div key={index}>
                    <div
                        style={
                            {
                                backgroundImage: 'url(' +api_files_path + slider.foto[0].imagen_file + ')',
                                backgroundPosition: 'center center',
                                backgroundRepeat: 'not-repeat',
                                backgroundSize: 'cover',
                            }
                        }
                        >
                        <div className="container">
                            <div style={{minHeight: '50vh'}} className="d-flex justify-content-between align-items-center">
                                <div>
                                    {/* <span>{slider.antetitulo}</span>
                                    <h1 className="my-0">{slider.titulo}</h1> */}
                                </div>
                                <div>
                                    <Paper style={{backgroundColor: secondaryColor}} className="p-5">
                                        <span className="badge badge-info">Día del niño</span>
                                        <h4>{slider.titulo}</h4>
                                        <Button color="orange" onClick={() => Router.push(`/producto/${slider.id}`)}>{slider.antetitulo}</Button>
                                    </Paper>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            ))}
        </Slider>
    )
}

Banner.propTypes = {
    //
}

Banner.getInitialProps = async () => {
    //
}

export default Banner
