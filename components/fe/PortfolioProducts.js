import React, { PropTypes, useState } from "react";
import {
  Container,
  CardActionArea,
  CardMedia,
  CardContent,
  Typography,
} from "@material-ui/core";
import { api_files_path, secondaryColor, tercerColor } from "../../enviroment";
import { Segment, Image, Input, Button, Reveal } from "semantic-ui-react";
import Router from "next/router";

export default function PortfolioProducts(props) {
  const { newProducts, featuredProducts } = props;

  const [newProductsOpen, setNewProductsOpen] = useState(true);
  const [featuredProductsOpen, setFeaturedProductsOpen] = useState(false);

  const [activeItem, setActiveItem] = React.useState("1");

  // const [activeList, setActiveList] = React.useState({
  //     productosDestacados =
  // })

  const toggle = (tab) => {
    if (activeItem !== tab) {
      setActiveItem(tab);
    }
  };

  return (
    <Container maxWidth="lg" className="my-5">
      <div className="d-flex justify-content-center align-items-center my-5">
        <Button
          style={{ borderRadius: "70px" }}
          onClick={() => {
            setNewProductsOpen(true);
            setFeaturedProductsOpen(false);
          }}
          primary={newProductsOpen}
          className="product-tab-link text-decoration-none mr-2"
        >
          Productos nuevos
        </Button>
        <Button
          style={{ borderRadius: "70px" }}
          onClick={() => {
            setNewProductsOpen(false);
            setFeaturedProductsOpen(true);
          }}
          primary={featuredProductsOpen}
          className="product-tab-link text-decoration-none mr-2"
        >
          Productos destacados
        </Button>
      </div>
      <div className="row mx-0 justify-content-center align-items-center">
        {featuredProducts.map((product, index) => (
          <Segment
            hidden={!featuredProductsOpen}
            key={index}
            className="col-md-3 mb-3 mx-3"
          >
            <Reveal animated="move down">
              <Reveal.Content visible>
                <Image
                  src={api_files_path + product.fotos[0].imagen_file}
                  wrapped
                  className="mb-3"
                />
              </Reveal.Content>
              <Reveal.Content
                hidden
                className="d-flex justify-content-center align-items-center h-100"
              >
                <dimmer
                  dimmed
                  style={{ opacity: 0.5 }}
                  className="d-flex h-100"
                >
                  <Image
                    src={api_files_path + product.fotos[0].imagen_file}
                    wrapped
                    className="mb-3"
                  />
                </dimmer>
                <Button
                  primary
                  style={{ position: "absolute" }}
                  onClick={() => Router.push(`producto/${product.id}`)}
                >
                  Ver producto
                </Button>
              </Reveal.Content>
            </Reveal>
            <small>{product.titulo}</small>
            <br />
            <small style={{ color: tercerColor }}>
              {product.categoria.rubro.rubro},{" "}
              {product.categoria.subrubro.subrubro},{" "}
              {product.categoria.subsubrubro.subsubrubro}
            </small>
            {product.precios.precio_lista !== "0" ? (
              <div className="d-flex align-items-end">
                <h3 className="mb-0 mr-2 text-gray">
                  <s>{product.precios.precio_lista}</s>
                </h3>
                <h2 className="mt-0">{product.precios.precio_venta}</h2>
              </div>
            ) : (
              <div className="d-flex">
                <h2 className="mt-0">{product.precios.precio_venta}</h2>
              </div>
            )}
            <div className="d-flex mt-3 justify-content-between align-items-center">
              <Input size="small" value="1">
                <Button
                  className="mr-0"
                  size="small"
                  icon="minus"
                  onClick={() => {
                    let input = document.getElementById("input-" + product.id);
                    if (input.value > 0) {
                      input.value = input.value - 1;
                    }
                  }}
                />
                <input id={"input-" + product.id} style={{ width: 40 }} />
                <Button
                  size="small"
                  icon="plus"
                  onClick={() => {
                    let input = document.getElementById("input-" + product.id);
                    if (input.value <= product.stock) {
                      input.value = Number(input.value) + 1;
                    }
                  }}
                />
              </Input>
              <Button size="small" icon="shopping basket" />
              <Button size="small" primary>
                Comprar
              </Button>
            </div>
          </Segment>
        ))}
        {newProducts.map((product, index) => (
          <Segment
            hidden={!newProductsOpen}
            key={index}
            className="col-md-3 mb-3 mx-3"
          >
            <Reveal animated="move down">
              <Reveal.Content visible>
                <Image
                  src={api_files_path + product.fotos[0].imagen_file}
                  wrapped
                  className="mb-3"
                />
              </Reveal.Content>
              <Reveal.Content
                hidden
                className="d-flex justify-content-center align-items-center h-100"
              >
                <dimmer
                  dimmed
                  style={{ opacity: 0.5 }}
                  className="d-flex h-100"
                >
                  <Image
                    src={api_files_path + product.fotos[0].imagen_file}
                    wrapped
                    className="mb-3"
                  />
                </dimmer>
                <Button
                  primary
                  style={{ position: "absolute" }}
                  onClick={() => Router.push(`/producto/${product.id}`)}
                >
                  Ver producto
                </Button>
              </Reveal.Content>
            </Reveal>
            <small>{product.titulo}</small>
            <br />
            <small style={{ color: tercerColor }}>
              {product.categoria.rubro.rubro},{" "}
              {product.categoria.subrubro.subrubro},{" "}
              {product.categoria.subsubrubro.subsubrubro}
            </small>
            {product.precios.precio_lista !== "0" ? (
              <div className="d-flex align-items-end">
                <h3 className="mb-0 mr-2 text-gray">
                  <s>{product.precios.precio_lista}</s>
                </h3>
                <h2 className="mt-0">{product.precios.precio_venta}</h2>
              </div>
            ) : (
              <div className="d-flex">
                <h2 className="mt-0">{product.precios.precio_venta}</h2>
              </div>
            )}
            <div className="d-flex mt-3 justify-content-between align-items-center">
              <Input size="small" value="1">
                <Button
                  className="mr-0"
                  size="small"
                  icon="minus"
                  onClick={() => {
                    let input = document.getElementById("input-" + product.id);
                    if (input.value > 0) {
                      input.value = input.value - 1;
                    }
                  }}
                />
                <input id={"input-" + product.id} style={{ width: 40 }} />
                <Button
                  size="small"
                  icon="plus"
                  onClick={() => {
                    let input = document.getElementById("input-" + product.id);
                    if (input.value <= product.stock) {
                      input.value = Number(input.value) + 1;
                    }
                  }}
                />
              </Input>
              <Button size="small" icon="shopping basket" />
              <Button size="small" primary>
                Comprar
              </Button>
            </div>
          </Segment>
        ))}
      </div>
    </Container>
  );
}
