import { useState, useEffect, useCallback } from 'react'
import Router, { useRouter } from 'next/router'
import Axios from 'axios'
import { api, principalTheme, links, api_files_path } from '../../enviroment'
import { ThemeProvider, Badge } from '@material-ui/core'
import Head from 'next/head'
import HeaderAD from '../../components/HeaderAD'
import Header from '../../components/fe/Header'
import ImageGallery from 'react-image-gallery'
import NumberFormat from 'react-number-format'
import { Input, Button } from 'semantic-ui-react'

function index({ menu, products }) {
    const [navTags, setNavTags] = useState([
        { title: "Día del niño", type: "info" },
        { title: "Oferta", type: "danger" },
        { title: "Día de playa", type: "info" },
    ]);
    
    const router = useRouter()
    const { id } = router.query

    const [product, setProduct] = useState({})
    const [precios, setPrecios] = useState({})
    const [stock, setStock] = useState({})
    const [images, setImages] = useState([])
    const [rubro, setRubro] = useState({})
    const [subRubro, setSubRubro] = useState({})
    const [subSubRubro, setSubSubRubro] = useState({})

    // color picker
    const [colors, setColor] = useState({})

    const getProduct = useCallback( async (id) => {
        const params = `id=${id}&id_edicion=MOD_PRODUCT_FILTER&edicion=productos&id_idioma=1&id_moneda=1`
        await Axios.get(api('producto', params)).then(response => {
            const data = response.data.data
            setProduct(data.producto)
            setPrecios(data.precios)
            setStock(data.stock)
            setRubro(data.categoria.rubro)
            setSubRubro(data.categoria.subrubro)
            setSubSubRubro(data.categoria.subsubrubro)
            const images = []
            data.fotos.forEach(foto => {
                images.push(
                    {
                        original: api_files_path + foto.imagen_file,
                        thumbnail: api_files_path + foto.imagen_file
                    }
                )
            })
            setImages(images)
        }).catch(errors => console.log('get product error:', errors))
    }, [])

    useEffect(() => {
        getProduct(id)
    }, [id])

    const handleTitle = () => {
        if (product) {
            return `Producto ${product.nombre}`
        }
        return 'loading...'
    }

    return (
        <ThemeProvider theme={principalTheme}>
            <Head>
                <title>{handleTitle()}</title>
                {links.map((link, index) => (
                <link
                    key={index}
                    rel="stylesheet"
                    href={link.url}
                    integrity={link.integrity || null}
                    crossOrigin={link.crossorigin || null}
                />
                ))}
            </Head>
            <HeaderAD title="Días del Niño!" hasButton buttonText="más información">
                Ahorra hasta un 50% Compra sin moverte de casa!
            </HeaderAD>
            <Header
                getMenu={menu}
                tags={navTags}
                search={products}
                logoURL="../assets/img/brand/logo.png"
            />
            <div className="container my-5">
                <div className="row justify-content-center">
                    <div className="col-lg-7">
                        <ImageGallery
                            thumbnailPosition="left"
                            bulletClass="bullet-product-image"
                            showPlayButton={false}
                            items={images}
                        />
                    </div>
                    <div className="col-lg-5">
                        <Badge>{product.estado}</Badge>
                        <h1 className="mt-0">{product.nombre}</h1>
                        <p>{product.text}</p>
                        <div className="d-flex">
                            {rubro?(
                                <a href="#">{rubro.rubro}</a>
                            ):null}{subRubro?', ':null}
                            {subRubro?(
                                <a href="#" className="ml-2">{subRubro.subrubro}</a>
                            ):null}{subSubRubro?', ':null}
                            {subSubRubro?(
                                <a href="#" className="ml-2">{subSubRubro.subsubrubro}</a>
                            ):null}
                        </div>
                        {precios.precio_lista !== "0" ? (
                        <div className="d-flex align-items-end">
                            <h3 className="mb-0 mr-2 text-gray">
                            <s>
                                <NumberFormat
                                    prefix={'$'}
                                    displayType={'text'}
                                    thousandSeparator={'.'}
                                    decimalSeparator={','}
                                    value={Number(precios.precio_lista)}
                                /></s>
                            </h3>
                            <h1 className="mt-0">
                                <NumberFormat
                                    prefix={'$'}
                                    displayType={'text'}
                                    thousandSeparator={'.'}
                                    decimalSeparator={','}
                                    value={Number(precios.precio_venta)}
                                />
                            </h1>
                        </div>
                        ) : (
                        <div className="d-flex">
                            <h1 className="mt-0">
                                <NumberFormat
                                    prefix={'$'}
                                    displayType={'text'}
                                    thousandSeparator={'.'}
                                    decimalSeparator={','}
                                    value={Number(precios.precio_venta)}
                                />
                            </h1>
                        </div>
                        )}
                        <h5>{stock.total > 0 ? 'Stock disponible' : 'Stock agotado'}</h5>
                        <div className="d-flex mt-5 justify-content-between align-items-center">
                            <Input size="small" value="1">
                                <Button
                                className="mr-0"
                                size="small"
                                icon="minus"
                                onClick={() => {
                                    let input = document.getElementById("input-" + product.id);
                                    if (input.value > 0) {
                                    input.value = input.value - 1;
                                    }
                                }}
                                />
                                <input id={"input-" + product.id} style={{ width: 40 }} />
                                <Button
                                    size="small"
                                    icon="plus"
                                    onClick={() => {
                                        let input = document.getElementById("input-" + product.id);
                                        if (input.value < Number(stock.total)) {
                                            input.value = Number(input.value) + 1;
                                        }
                                    }}
                                />
                            </Input>
                            <div className="d-flex align-items-center">
                                <Button disabled={!stock.total > 0} size="small" icon="shopping basket" />
                                <Button disabled={!stock.total > 0} size="small" primary>
                                    Comprar
                                </Button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </ThemeProvider>
    )
}

index.getInitialProps = async () => {
    const menuParams = "id_edicion=MOD_PRODUCT_FILTER&edicion=productos";
    const menuRes = await fetch(api("menu", menuParams));
    const menuJson = await menuRes.json();

    const productsParams =
      "id_idioma=1&id_moneda=1&id_edicion=MOD_PRODUCT_FILTER&edicion=productos&fotos=2&order=orden&dir=ASC&iDisplayLength=30&iDisplayStart=0&limit=8&filtros%5Bdestacado%5D=1";
    const productsRes = await fetch(api("listadoProductos", productsParams));
    const productsJson = await productsRes.json();

    return {
      menu: menuJson.data.rubros,
      products: productsJson.data.productos,
    }
}

export default index