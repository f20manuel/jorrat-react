import * as React from 'react'
import Head from 'next/head'
import { links } from '../../../enviroment'
import Header from '../../../components/we-admin/Header'

export default function dashboard() {
    return (
        <>
            <Head>
                <title>Escriorio</title>
                {links.map((link, index) => (
                    <link key={index} rel="stylesheet" href={link.url} integrity={link.integrity || null} crossorigin={link.crossorigin || null} />
                ))}
            </Head>
            <Header></Header>
        </>
    )
}