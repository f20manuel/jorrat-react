import React from 'react'
import { Button, Form, Grid, Header, Image, Message, Segment } from 'semantic-ui-react'
import Head from 'next/head'
import { links } from '../../enviroment'

export default function LoginForm() {
    const [loginData, setLoginData] = React.useState({
        email: '',
        password: '',
    })

    const handleLoginData = event => {
        setLoginData({
            ...loginData,
            [event.target.name]: event.target.value
        })
    }

    const handleSubmit = () => {
        //
    }

    return (
        <>
            <Head>
                <title>Admin Panel</title>
                {links.map((link, index) => (
                    <link key={index} rel="stylesheet" href={link.url} integrity={link.integrity || null} crossorigin={link.crossorigin || null} />
                ))}
            </Head> 
            <Grid textAlign='center' style={{ height: '100vh' }} verticalAlign='middle'>
                <Grid.Column style={{ maxWidth: 450 }}>
                <Header as='h2' color='teal' textAlign='center'>
                    <Image src='/logo.png' /> Log-in to your account
                </Header>
                <Form size='large'>
                    <Segment stacked>
                    <Form.Input
                        value={loginData.email}
                        type="email"
                        id="email"
                        name="email"
                        fluid icon='user'
                        iconPosition='left'
                        placeholder='Correo electrónico'
                        onChange={event => handleLoginData(event)}
                    />
                    <Form.Input
                        value={loginData.password}
                        id="password"
                        name="password"
                        fluid
                        icon='lock'
                        iconPosition='left'
                        placeholder='Contraseña'
                        type='password'
                        onChange={event => handleLoginData(event)}
                    />

                    <Button color='green' fluid size='large'>
                        Login
                    </Button>
                    </Segment>
                </Form>
                <Message>
                    ¿No tienes cuenta? <a href='#'>Crear cuenta</a>
                </Message>
                </Grid.Column>
            </Grid>
        </>
    )
}