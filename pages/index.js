import * as React from "react";
import * as Axios from "axios";
import Head from "next/head";
import {
  links,
  principalTheme,
  public_path,
  secondaryColor,
  cuartoColor,
  api,
  sldierBrandConfig,
  api_files_path,
} from "../enviroment";
import HeaderAD from "../components/HeaderAD";
import { ThemeProvider, Container } from "@material-ui/core";
import Header from "../components/fe/Header";
import Banner from "../components/fe/Banner";
import Features from "../components/fe/Features";
import Ages from "../components/fe/Ages";
import PortfolioProducts from "../components/fe/PortfolioProducts";
import { Image } from "semantic-ui-react";
import Footer from "../components/fe/Footer";
import Slider from "react-slick";

function Home(props) {
  const {
    menu,
    products,
    newProducts,
    featuredProducts,
    sliders,
    features,
    banner1,
    banner2,
    banner3,
    banner4,
    banner5,
    banner6,
    marcas,
    address,
  } = props;

  const [navTags, setNavTags] = React.useState([
    { title: "Día del niño", type: "info" },
    { title: "Oferta", type: "danger" },
    { title: "Día de playa", type: "info" },
  ]);

  const [ages, setAges] = React.useState([
    {
      title: banner1.nombre.split(" ")[0],
      type: banner1.nombre.split(" ")[1],
      buttonText: "Ver productos",
      buttonLink: banner1.link,
      backgroundImage: `${api_files_path}banners/${banner1.banners}`,
    },
    {
      title: banner2.nombre.split(" ")[0],
      type: banner2.nombre.split(" ")[1],
      buttonText: "Ver productos",
      buttonLink: banner2.link,
      backgroundImage: `${api_files_path}banners/${banner2.banners}`,
    },
    {
      title: banner3.nombre.split(" ")[0],
      type: banner3.nombre.split(" ")[1],
      buttonText: "Ver productos",
      buttonLink: banner3.link,
      backgroundImage: `${api_files_path}banners/${banner3.banners}`,
    },
  ]);

  return (
    <ThemeProvider theme={principalTheme}>
      <Head>
        <title>Jorrat</title>
        {links.map((link, index) => (
          <link
            key={index}
            rel="stylesheet"
            href={link.url}
            integrity={link.integrity || null}
            crossOrigin={link.crossorigin || null}
          />
        ))}
        {/* <script src="https://kit.fontawesome.com/0c3e8c9d69.js" crossorigin="anonymous"></script> */}
      </Head>
      <HeaderAD title="Días del Niño!" hasButton buttonText="más información">
        Ahorra hasta un 50% Compra sin moverte de casa!
      </HeaderAD>
      <Header
        getMenu={menu}
        tags={navTags}
        search={products}
        logoURL="./assets/img/brand/logo.png"
      />
      <Banner sliders={sliders} />
      <Features data={features} />
      <Ages
        data={ages}
        title="La mayor variedad y los precios más bajos de juguetes para niños"
      />
      <PortfolioProducts
        newProducts={newProducts}
        featuredProducts={featuredProducts}
      />
      <Container maxWidth="md">
        <div className="row justify-content-center mx-0">
          {banner4.habilitado ? (
            <div
              style={{
                backgroundImage:
                  "url(" + api_files_path + "banners/" + banner4.banners + ")",
                backgroundRepeat: "no-repeat",
                backgroundSize: "cover",
                backgroundPosition: "center center",
              }}
              className="col-12 mb-3"
            >
              <div className="col-md-6 p-4 p-md-5">
                <h1
                  style={{ color: secondaryColor }}
                  className="title text-uppercase mb-0"
                >
                  {banner4.nombre}
                </h1>
                <h5
                  style={{ color: secondaryColor, marginBottom: "40px" }}
                  className="subtitle mt-0"
                >
                  Todo los productos para pasar un Domingo en la pile
                </h5>
                <div className="my-2">
                  <a
                    href="#"
                    className="product-tab-link bg-secondary border-0 py-3 px-4 mt-3 text-decoration-none mr-2"
                  >
                    COMPRAR
                  </a>
                </div>
              </div>
            </div>
          ) : null}
          {banner5.habilitado ? (
            <div className="col-md-6 pl-md-0 pr-md-2 px-0 mb-3">
              <div
                style={{
                  backgroundImage:
                    "url(" +
                    api_files_path +
                    "banners/" +
                    banner5.banners +
                    ")",
                  backgroundRepeat: "no-repeat",
                  backgroundSize: "cover",
                  backgroundPosition: "center center",
                }}
                className="p-4"
              >
                <h1
                  style={{ color: secondaryColor, color: "white" }}
                  className="title mb-0"
                >
                  ¿DE VIAJE?
                </h1>
                <h5
                  style={{
                    color: secondaryColor,
                    color: "white",
                    marginBottom: "50px",
                  }}
                  className="subtitle mt-0"
                >
                  Mirá los juguetes que no te pueden faltar para el viaje
                </h5>
                <div className="my-2">
                  <a
                    href="#"
                    className="product-tab-link bg-secondary border-0 py-3 px-4 mt-3 text-decoration-none mr-2"
                  >
                    COMPRAR
                  </a>
                </div>
              </div>
            </div>
          ) : null}
          {banner6.habilidato ? (
            <div className="col-md-6 pr-md-0 pl-md-2 px-0 mb-3">
              <div
                style={{
                  backgroundImage:
                    "url(" +
                    api_files_path +
                    "banners/" +
                    banner6.banners +
                    ")",
                  backgroundRepeat: "no-repeat",
                  backgroundSize: "cover",
                  backgroundPosition: "center center",
                }}
                className="p-4"
              >
                <h1
                  style={{ color: secondaryColor, color: "white" }}
                  className="title mb-0"
                >
                  ¡PIJAMA NIGHT!
                </h1>
                <h5
                  style={{
                    color: secondaryColor,
                    color: "white",
                    marginBottom: "50px",
                  }}
                  className="subtitle mt-0"
                >
                  Colección de juegos de mesa para que la pases bomba!
                </h5>
                <div className="my-2">
                  <a
                    href="#"
                    className="product-tab-link bg-secondary border-0 py-3 px-4 mt-3 text-decoration-none mr-2"
                  >
                    COMPRAR
                  </a>
                </div>
              </div>
            </div>
          ) : null}
        </div>
      </Container>
      <div
        style={{ backgroundColor: cuartoColor }}
        className="w-100 p-5 mt-5 mb-n5"
      >
        <Container maxWidth="md">
          <Slider {...sldierBrandConfig}>
            {marcas.map((marca, index) => (
              <div key={index}>
                <Image
                  src={api_files_path + marca.fotos[0].imagen_file}
                  style={{ height: 100 }}
                />
              </div>
            ))}
          </Slider>
        </Container>
      </div>
      <Footer address={address} />
    </ThemeProvider>
  );
}

Home.getInitialProps = async () => {
  const menuParams = "id_edicion=MOD_PRODUCT_FILTER&edicion=productos";
  const menuRes = await fetch(api("menu", menuParams));
  const menuJson = await menuRes.json();

  const productsParams =
    "id_idioma=1&id_moneda=1&id_edicion=MOD_PRODUCT_FILTER&edicion=productos&fotos=2&order=orden&dir=ASC&iDisplayLength=30&iDisplayStart=0&limit=8&filtros%5Bdestacado%5D=1";
  const productsRes = await fetch(api("listadoProductos", productsParams));
  const productsJson = await productsRes.json();

  const newProductsParams =
    "id_idioma=1&id_moneda=1&id_edicion=MOD_PRODUCT_FILTER&edicion=productos&fotos=1&order=orden&dir=ASC&iDisplayLength=30&iDisplayStart=0&limit=21&mas_vistos=1";
  const newProductsRes = await fetch(
    api("listadoProductos", newProductsParams)
  );
  const newProductsJson = await newProductsRes.json();

  const featureProductsParams =
    "id_idioma=1&id_moneda=1&id_edicion=MOD_PRODUCT_FILTER&edicion=productos&fotos=2&order=orden&dir=ASC&iDisplayLength=30&iDisplayStart=0&limit=21&destacado=1";
  const featureProductsRes = await fetch(
    api("listadoProductos", featureProductsParams)
  );
  const featureProductsJson = await featureProductsRes.json();

  const slidersParams =
    "&id_edicion=MOD_NEWSSLIDER_FILTER&edicion=slider&fotos=99";
  const slidersRes = await fetch(api("slider", slidersParams));
  const slidersJson = await slidersRes.json();

  const featuresParams =
    "id_edicion=MOD_SERVICE_FILTER&edicion=news&fotos=0&iDisplayLength=99&iDisplayStart=0&habilitado=1";
  const featuresRes = await fetch(api("listadoNotas", featuresParams));
  const featuresJson = await featuresRes.json();

  const banner1Params = "edicion=banners&id=1";
  const banner1Res = await fetch(api("banners_front", banner1Params));
  const banner1Json = await banner1Res.json();

  const banner2Params = "edicion=banners&id=2";
  const banner2Res = await fetch(api("banners_front", banner2Params));
  const banner2Json = await banner2Res.json();

  const banner3Params = "edicion=banners&id=3";
  const banner3Res = await fetch(api("banners_front", banner3Params));
  const banner3Json = await banner3Res.json();

  const banner4Params = "edicion=banners&id=4";
  const banner4Res = await fetch(api("banners_front", banner4Params));
  const banner4Json = await banner4Res.json();

  const banner5Params = "edicion=banners&id=5";
  const banner5Res = await fetch(api("banners_front", banner5Params));
  const banner5Json = await banner5Res.json();

  const banner6Params = "edicion=banners&id=6";
  const banner6Res = await fetch(api("banners_front", banner6Params));
  const banner6Json = await banner6Res.json();

  const marcasParams =
    "id_edicion=MOD_PRODUCT_FILTER&fotos=1&edicion=marcas&destacado=1";
  const marcasRes = await fetch(api("listadoMarcas", marcasParams));
  const marcasJson = await marcasRes.json();

  const addressParams =
    "edicion=news&fotos=0&iDisplayLength=99&iDisplayStart=0&id_edicion=MOD_SUCURSALES_FILTER&id_seccion=3";
  const addressRes = await fetch(api("listadoNotas", addressParams));
  const addressJson = await addressRes.json();

  return {
    menu: menuJson.data.rubros,
    products: productsJson.data.productos,
    newProducts: newProductsJson.data.productos,
    featuredProducts: featureProductsJson.data.productos,
    sliders: slidersJson.data,
    features: featuresJson.data.nota,
    banner1: banner1Json.data.banner,
    banner2: banner2Json.data.banner,
    banner3: banner3Json.data.banner,
    banner4: banner4Json.data.banner,
    banner5: banner5Json.data.banner,
    banner6: banner6Json.data.banner,
    marcas: marcasJson.data,
    address: addressJson.data.nota,
  };
};

export default Home;
