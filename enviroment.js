import { createMuiTheme } from "@material-ui/core";

export const links = [
    {url: 'https://fonts.googleapis.com/css2?family=Raleway:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap'},
    {url: 'https://fonts.googleapis.com/css2?family=Londrina+Solid:wght@100;300;400;900&display=swap'},
    {
        url: 'https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css',
        integrity: "sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk",
        crossorigin: "anonymous"
    },
    {url: '//cdn.jsdelivr.net/npm/semantic-ui@2.4.2/dist/semantic.min.css'},
    {url: 'https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick.min.css'},
    {url: 'https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick-theme.min.css'},
    {url: 'assets/css/styles.css'}
]

export const base_url = 'http://localhost:3000/'

export function public_path(url = false) {
    if (url) return base_url + url
    return base_url
}

export const domainDev = 'http://wserver.com.ar/'

export const domainPro = 'https://jorrat.com.ar/' 

export const api_files_path = 'http://wserver.com.ar/jorrat/fe/public/uploads/'

export const api_url = domainDev + 'jorrat/wxpanel/public/frontClient/rest/v1/'

export function api(route, params = '') {
    let url = api_url +
              route + '?' +
              params 
    return url
}

export const primaryColor = '#FE324B'
export const secondaryColor = '#FFE463'
export const tercerColor = '#fcba48'
export const cuartoColor = '#F3F6EB'

export const principalTheme = createMuiTheme({
    palette: {
        primary: {
            main: primaryColor
        },
        secondary: {
            main: secondaryColor,
        },
    },
});

export const sliderPrincipalBanner = {
  dots: true,
  infinite: false,
  speed: 500,
  slidesToShow: 1,
  slidesToScroll: 1,
  initialSlide: 0,
};

export const sldierBrandConfig = {
    dots: true,
    infinite: false,
    speed: 500,
    slidesToShow: 8,
    slidesToScroll: 8,
    initialSlide: 0,
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 4,
          slidesToScroll: 4,
          infinite: true,
          dots: true
        }
      },
      {
        breakpoint: 600,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2,
          initialSlide: 2
        }
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      }
    ]
};